<?php

/**
 * @file
 * The mode module's administration functions.
 *
 * @author Karthik Kumar / Zen [ http://drupal.org/user/21209 ].
 */

/**
 * Menu callback: Mode module settings form.
 */
function mode_list() {
  $header = array(t('Name'), t('Status'), array('data' => t('Operations'), 'colspan' => 3));
  $rows = array();
  $modes = mode_mode_load();

  foreach ($modes as $mode) {
    $rows[] = array(
      check_plain($mode->name),
      $mode->active ? t('Enabled') : t('Disabled'),
      l(t('Edit'), 'admin/config/development/mode/edit/' . $mode->mid),
      l(t('Delete'), 'admin/config/development/mode/delete/' . $mode->mid),
      $mode->active ? l(t('Reapply'), 'admin/config/development/mode/switch/' . $mode->mid) : l(t('Apply'), 'admin/config/development/mode/switch/' . $mode->mid)
    );
  }

  return theme('table' , array('header' => $header, 'rows' => $rows));
}

/**
 * Menu callback: Mode settings form.
 */
function mode_settings_form($form, &$form_state) {
  $mode_config = _mode_variable_get();

  $form['mode_config'] = array('#tree' => TRUE);
  $form['mode_config']['banner'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display banner message'),
    '#description' => t('If enabled, the banner message associated with a mode will be displayed on all pages as a status message.'),
    '#default_value' => $mode_config['banner']
  );
  $form['mode_config']['permissions_page'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable permissions page'),
    '#description' => t('If enabled, the permissions page will be disabled along with a message stating that the mode settings page should be used instead.'),
    '#default_value' => $mode_config['permissions_page']
  );

  return system_settings_form($form);
}

/**
 * Menu callback: Mode add/edit form.
 */
function mode_edit_form($form, &$form_state, $mode_id = NULL) {
  $mode = $mode_id ? mode_mode_load($mode_id) : NULL;

  require_once(drupal_get_path('module', 'user') . '/user.admin.inc');

  $form = user_admin_permissions($form, $form_state);

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Give this mode a unique name for identification purposes.'),
    '#required' => TRUE,
    '#weight' => 1
  );
  $form['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Banner message'),
    '#description' => t('Status message to display on all pages when this mode is enabled. HTML can be used. A toggle switch is also available on the settings page to control display.'),
    '#rows' => 2,
    '#weight' => 2
  );
  // Due to the way this form is rendered in theme_user_admin_permissions, we
  // shove the name field to the bottom, but above the submit button.
  $form['submit']['#weight'] = 3;

  if ($mode) {
    drupal_set_title(t('Mode edit: !name', array('!name' => $mode->name)));

    $form['name']['#default_value'] = $mode->name;
    $form['message']['#default_value'] = $mode->message;
    foreach ($form['checkboxes'] as $rid => $options) {
      $defaults = isset($mode->data[$rid]) ? explode(', ', $mode->data[$rid]) : array();
      $form['checkboxes'][$rid]['#default_value'] = $defaults;
    }
    $form['mid'] = array('#type' => 'value', '#value' => $mode->mid);

    $form['submit']['#value'] = t('Update mode');
  }
  else {
    $form['submit']['#value'] = t('Add mode');
  }

  $form['#theme'] = 'user_admin_permissions';

  return $form;
}

/**
 * Process mode edit form submissions.
 */
function mode_edit_form_submit($form, &$form_state) {
  $form_values = $form_state['values'];
  $permissions = array();

  $result = db_query('SELECT * FROM {role}');
  foreach ($result as $role) {
    if (isset($form_values[$role->rid])) {
      $form_values[$role->rid] = array_filter($form_values[$role->rid]);
      if (count($form_values[$role->rid])) {
        $permissions[$role->rid] = implode(', ', array_keys($form_values[$role->rid]));
      }
    }
  }

  $mode = array('name' => trim($form_values['name']), 'data' => serialize($permissions), 'message' => trim($form_values['message']));
  $update = array();
  if (isset($form_values['mid'])) {
    $mode['mid'] = $form_values['mid'];
    $update[] = 'mid';
  }
  $status = mode_mode_add($mode, $update);

  $form_state['redirect'] = 'admin/config/development/mode';
}

/**
 * Delete a mode.
 */
function mode_delete_form($form, &$form_state, $mode_id = NULL) {
  $mode = $mode_id ? mode_mode_load($mode_id) : NULL;
  $form['mode'] = array('#type' => 'value', '#value' => $mode);
  return confirm_form($form,
    t('Are you sure you want to delete the mode %name?', array('%name' => $mode->name)),
    'admin/config/development/mode',
    t('This will delete the mode permanently. This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Process mode delete form submissions.
 */
function mode_delete_form_submit($form, &$form_state) {
  mode_mode_delete($form_state['values']['mode']);
  $form_state['redirect'] = 'admin/config/development/mode';
}

/**
 * Confirmation form for mode switching.
 */
function mode_mode_switch_form($form, &$form_state, $mode_id) {
  $mode = mode_mode_load($mode_id);
  if (isset($mode->mid)) {
    $form['mode'] = array('#type' => 'value', '#value' => $mode);
    return confirm_form($form,
      t('Are you sure you want to switch to mode %name?', array('%name' => $mode->name)),
      'admin/config/development/mode',
      $mode->message,
      t('Switch'),
      t('Cancel')
    );
  }
  else {
    drupal_set_message(t('No such mode is available.'));
    drupal_goto('admin/config/development/mode');
  }
}

/**
 * Process mode delete form submissions.
 */
function mode_mode_switch_form_submit($form, &$form_state) {
  $mode = mode_mode_switch($form_state['values']['mode']->mid);

  if ($mode) {
    drupal_set_message(t('Mode switched to %mode.', array('%mode' => $mode->name)));
  }

  $form_state['redirect'] = 'admin/config/development/mode';
}
