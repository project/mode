<?php

/**
 * @file
 * Drush integration for the Mode module.
 */

/**
 * Implements hook_drush_help().
 */
function mode_drush_help($section) {
  if ($section == 'drush:mode-switch') {
    return dt('The mode module allows administrators to switch from one permission set to another. ');
  }
}

/**
 * Implements hook_drush_command().
 */
function mode_drush_command() {
  $items['mode-switch'] = array(
    'description' => 'Switch from one mode to another.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'mode' => 'The name of the mode'
    ),
    'aliases' => array('mosw'),
    'examples' => array(
      'drush mode-switch development' => 'Switch on a mode named development.',
      'drush mosw default' => 'Or the mosw alias can also be used.'
    )
  );

  $items['mode-list'] = array(
    'description' => 'List all available modes.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('moli'),
    'examples' => array(
      'drush mode-list' => 'List all modes.',
      'drush moli' => 'The same can be done with the moli alias.'
    )
  );

  return $items;
}

/**
 * Execute the Drush mosw command.
 */
function drush_mode_switch($mode_name = NULL) {
  if ($mode_name && $mode = mode_mode_switch($mode_name)) {
    drush_print(dt('Mode successfully switched.'));
    drush_print(dt('@message', array('@message' => $mode->message)));
  }
  else {
    drush_set_error('MODE_SWITCH_FAILED', dt('Mode switching failed.'));
  }
}

/**
 * Execute the Drush moli command.
 */
function drush_mode_list() {
  $modes = mode_mode_load();
  if (!empty($modes)) {
    $rows = array();
    foreach ($modes as $mode) {
      $rows[] = array(check_plain($mode->name) . ($mode->active ? '*' : ''));
    }

    drush_print(dt('Available modes:'));
    drush_print_table($rows);
  }
  else {
    drush_print(dt('No modes available.'));
  }
}
