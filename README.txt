Overview
--------

The Mode module is a utility module that allows administrators to manage
different permission sets and switch between them conveniently. It works by
manipulating the permissions table during each switch.

If the mode module is being used, it is highly recommended that permissions be
managed using its interface rather than the default system page.

Example scenarios where this module will prove useful:

- Setting the site to a read-only mode by creating a mode where users only have
  the 'access content' permission.
- Manually throttle access to certain modules such as search by limiting access
  to 'search content' and 'use advanced search'.
- Create permission templates for multi-site installations to ease 
  administration.

Notes
-----

- The module can be configured and managed via admin/config/development/mode.
- It is recommended that the original set of permissions be saved as a 'backup'.
- The settings tab has a toggle to determine if a status message is displayed
  on each page view when the associated mode is enabled.
- The settings tab also has an option that will disable the 
  admin/user/permissions form and inform the administrator of the presence of
  the mode module.
- The module provides a block with links to activate each permission set as 
  required.
- Only users with who can "administer permissions" have access to this module.
- The module can also be used via Drush. Type "drush help mode-switch" for more
  information.

Credits
-------

Author: Karthik Kumar / Zen [ http://drupal.org/user/21209 ]

Project URL: http://drupal.org/project/mode
